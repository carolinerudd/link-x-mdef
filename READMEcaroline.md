---
title: Will Coronavirus Crash the Internet?
author: Caroline Rudd
category: Energy
layout: post
---

# Will Coronavirus Crash the Internet?

#### By Caroline Rudd

**The world’s biggest online experiment has been triggered by Covid-19. Remote work, e-learning, video conferencing and online media consumption are growing rapidly, placing a huge strain on the internet’s infrastructure.
**

**According to preliminary statistics, total internet hits have surged between 50% and 70%. Because of this, the EU and industry experts have called on streaming services, operators and users to prevent internet overload, as no one knows when the pandemic will subside. Numerous companies including Netflix, YouTube, and TikTok have taken steps to reduce network congestion, such as reducing resolution and bitrate. As there’s already been multiple outages around the world, it’s important that users also take steps to ensure internet access for those fields that truly need it in these dire times, like medical professionals.
**
---

## Here are some data saving tips you can try from home

1. Switch to Standard Definition(SD) when High Definition(HD) isn't necessary while streaming videos.
2. Stop videos from automatically playing on Facebook in the "autoplay" section of Facebook's Video Settings.
3. Use wifi when possible, rather than cellular data.
4. Use smaller devices when possible, like a cellphone or tablet instead of a desktop.
5. Completely shut off devices when you're not using them, rather than just enter sleep mode.

## Here's how to check your home broadband speed (this may be important if you have a big meeting or test coming up and need to ensure your connection is stable, or if you want to check that your internet service is providing you with the appropriate speed you're paying for.)

1. Connect your computer to Wi-Fi or use a router using an ethernet cable.
2. Open any browser.
3. Navigate to Speedtest.net


#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
