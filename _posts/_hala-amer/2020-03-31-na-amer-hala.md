---
title: Resilience or Resistance
author: Hala Amer
category: Education
layout: post
---

# The Shape of Necessity

### Introduction to Project by Tomas
The Master in Design for Emergent Futures is proposing a revision of the publication made by the Cuban government during the 1990's, when the island was going through the special period after the fall of the Iron Curtain. MDEF's tutors and students will work in a new repository and publication, understanding that the world is now in a new "special period", after the release of the COVID-19.

Knowing the limitations of mobility that the world is experiencing, there is a dramatic change in logistics and supply chains, as well as new ways to access knowledge. Right now, we are more connected than ever and we are more isolated than ever at the same time. The current global pandemic is opening up opportunities for hyperlocal interventions that can help people to live confinement in much better conditions while increasing long term resilience.

The exercise will consist of mapping and documenting existing practices that are happening now at the domestic scale in the world, and which people are using to solve local needs, by following open source projects or existing documentation. The work will be organized following the categories of the book "Con Nuestros Propios Esfuerzos".

These categories will be assigned to students by MDEF tutors, and they will serve as a starting point to organize their work for the week. A second part of the exercise will consist of imagining near-future scenarios, starting from January 2021, and proposing the evolution of these categories and projects in the context of the Post-Corona society. We want our students to identify not only the current situation of confinement but also being able to anticipate the world that could emerge out of this forced transition. Starting from January 2021, we want our students to make an effort to identify what these collective practices or this domestic practices could look like in six or nine months.

Ernesto Oroza, who is going to be a collaborator in this week's exercise, will participate in the collective efforts by inspiring students in how to curate and identify which kind of solutions or which kind of recipes are being developed and shared around the world as never before.

Objectives:

- to document do-it-yourself practices that are emerging at the domestic space from people that are solving basic needs related with food, fixtures of electronics, clothing, or fixing anything that is in their homes

- to curate together with MDEF tutors a collection of solutions and projects that are offering opportunities to address the current reconfiguration of the everyday life of humans in the world of confinement

- to speculate about possible near futures after the Corona pandemic, starting in January 2021

The output expected from this project this week project is to have a living repository of solutions or ideas or creative ideas for people to implement other domestic spaces as well as to think about how a new productive society might be emerging from this current crisis of the Coronavirus, and how we can envision this productive society to finally emerge from 2020 as a transition year.

---

# Delivery Services

#### Hala Amer

**Introductory statement.**
**~100 characters**

**Descriptive text about sub-focus. ~300 characters**

---

## Idea 1: Bread

After the lockdown in Jordan, the government enlisted a schedule to allow for bread to be delivered to each household to encourage staying at home. In the course of two days, a delivery schedule was established, buses and bus drivers were recruited, and panaderos were tasked with specific numbers of bread to have ready.
The rounds began with buses stopping at different neighborhoods and a police force standing outside each bus to make sure people followed the required rules of standing in line. The rules included leaving distances of 4 meters between people and wearing masks and gloves. The results varied from different neighborhoods.

![Bread Delivery](bus-bread.jpeg) Delivery of the bread on buses
![Following Rules](following.jpeg) Military making sure citizens follow the mandated 4 meters 
![Not Following Rules](bread.jpeg) Failure of citizens realizing the danger in the panic

## Idea 2: Alcohol

Beginning before the pandemic and thriving through the lockdown is an alcohol delivery application in Jordan. "Botler is Jordan’s largest digital marketplace for alcohol, connecting drinkers to suppliers, giving our customers the best way to shop for their drinks."

[BotlerJo Website](https://botlerjo.com/)
![App Store](botlerjo.jpeg)


## Idea 3: Groceries

Mouneh - an app that allows you to find the closest grocery stores to your location to minimize wandering outdoors. Once the location is found one needs to print out a paper that is signed with the house location and the supermarkets location, to make sure that the citizens are following the shortest routes.

![Mouneh App](mouneh.JPG)

Previously existing delivery services include CareemBox and eButler.
![Careem](careem.JPG)"Careem is the internet platform for the greater Middle East region. A pioneer of the region's ride-hailing economy, Careem is expanding services across is platform to include mass transportation, delivery and payments to become the region's everyday SuperApp."
![eButler](ebutler.JPG)"EButler is the most reliable services providers, having an All-in-One app on your phone that provides you with everything you need in your personal life. "

## Idea 4: Help

> Don't panic
>> Inline block quote.


## Idea 5: A beginner's guide to Linux OS
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
s = "Python syntax highlighting"
print s
```

```
No language indicated, so no syntax highlighting.
But let's throw in a <b>tag</b>.
```

#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
